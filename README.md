## Description
Food delivery api

## Running the application (need docker to be installed)

```bash
$ npm run start:docker
```

## Swagger page

http://localhost:3001/swagger