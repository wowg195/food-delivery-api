import { Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { path } from 'app-root-path';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ScheduleModule } from '@nestjs/schedule';

import { ProductsModule } from './modules/products/products.module';
import { ToppingModule } from './modules/topping/topping.module';
import { ToppingCategoriesModule } from './modules/topping-categories/topping-categories.module';
import { ActionsModule } from './modules/action/action.module';
import { ProductsCategoriesModule } from './modules/products-categories/products-categories.module';
import { ContentsModule } from './modules/contents/contents.module';
import { ComponentsModule } from './modules/component/components.module';
import { ComponentsCategoriesModule } from './modules/components-categories/components-categories.module';
import { FilesModule } from './modules/files/files.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { CitiesModule } from './modules/cities/city.module';
import { PointsModule } from './modules/points/points.module';
import { CartModule } from './modules/cart/cart.module';
import { ComponentsItemsModule } from './modules/components-items/components-items.module';
import { RedisCacheModule } from './modules/redis-cache/redis-cache.module';
import { RequestManagerModule } from './modules/request-manager/request-manager.module';
import { IikoModule } from './modules/iiko/iiko.module';
import { UserModule } from './modules/user/user.module';
import { AuthModule } from './modules/auth/auth.module';
import { CronModule } from './modules/cron/cron.module';
import { PUBLIC_FOLDER_PATH } from './config/config';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    ScheduleModule.forRoot(),
    MongooseModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService): MongooseModuleOptions => ({
        uri:
          'mongodb://' +
          configService.get('MONGODB_USER') +
          ':' +
          configService.get('MONGODB_USER_PASSWORD') +
          '@' +
          configService.get('MONGODB_URL'),
      }),
    }),
    ServeStaticModule.forRoot({
      rootPath: `${path}/uploads`,
      serveRoot: PUBLIC_FOLDER_PATH,
      serveStaticOptions: {
        index: false,
      },
    }),
    ProductsModule,
    ToppingModule,
    ToppingCategoriesModule,
    ActionsModule,
    ProductsCategoriesModule,
    ContentsModule,
    ComponentsModule,
    ComponentsCategoriesModule,
    FilesModule,
    CitiesModule,
    PointsModule,
    CartModule,
    ComponentsItemsModule,
    RedisCacheModule,
    RequestManagerModule,
    IikoModule,
    UserModule,
    AuthModule,
    CronModule,
  ],
})
export class AppModule {}
