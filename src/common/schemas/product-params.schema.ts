import { Prop } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

export class ProductParamsSchema {
  @Prop({ default: 0 })
  @ApiProperty()
  weight: number;
}
