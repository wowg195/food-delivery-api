import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import {
  ApiTags,
  ApiOperation,
  ApiResponse,
  ApiParam,
  ApiBearerAuth,
  ApiConsumes,
} from '@nestjs/swagger';

import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { ActionsService } from './action.service';
import { CreateActionDto } from './dto/createAction.dto';
import { ActionEntity } from './schemas/action.schema';

@ApiTags('actions')
@Controller('actions')
export class ActionsController {
  constructor(private readonly actionsService: ActionsService) {}

  @Get()
  @ApiOperation({ summary: 'Get all actions' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [ActionEntity],
  })
  async getActions(): Promise<ActionEntity[] | null> {
    return this.actionsService.getActions();
  }

  @Get(':actionId')
  @ApiOperation({ summary: 'Get action by id' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: ActionEntity,
  })
  @ApiParam({
    name: 'actionId',
    required: true,
    description: 'Action id',
  })
  async getActionById(
    @Param('actionId') actionId: string,
  ): Promise<ActionEntity> {
    return this.actionsService.getActionById(actionId);
  }

  @Post()
  @ApiOperation({ summary: 'Create action' })
  @ApiResponse({
    status: 201,
    description: 'Success',
    type: ActionEntity,
  })
  @ApiConsumes('multipart/form-data')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('image'))
  async createAction(
    @UploadedFile() file: Express.Multer.File,
    @Body() createActionDto: CreateActionDto,
  ): Promise<ActionEntity> {
    return this.actionsService.createAction(createActionDto, file);
  }
}
