import { MongooseModule } from '@nestjs/mongoose';
import { Module } from '@nestjs/common';

import { ActionEntity, ActionSchema } from './schemas/action.schema';
import { ActionsController } from './action.controller';
import { ActionsService } from './action.service';
import { ActionsRepository } from './action.repository';
import { FilesModule } from '../files/files.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: ActionEntity.name, schema: ActionSchema },
    ]),
    FilesModule,
  ],
  controllers: [ActionsController],
  providers: [ActionsService, ActionsRepository],
})
export class ActionsModule {}
