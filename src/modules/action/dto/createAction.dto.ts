import { ApiProperty } from '@nestjs/swagger';
import { Transform, Type } from 'class-transformer';
import { IsBoolean, IsNumber, IsOptional, IsString } from 'class-validator';
import { Express } from 'express';

export class CreateActionDto {
  @IsString()
  @ApiProperty()
  readonly title: string;

  @IsNumber()
  @IsOptional()
  @Type(() => Number)
  @ApiProperty({ required: false })
  readonly order?: number;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  readonly description?: string;

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  readonly emoji?: string;

  @IsBoolean()
  @IsOptional()
  @Transform(({ value }) => {
    return [true, 'true', 1, '1'].indexOf(value) > -1;
  })
  @ApiProperty()
  readonly status?: boolean;

  @IsOptional()
  @ApiProperty({ type: 'string', format: 'binary' })
  image: Express.Multer.File;
}
