import { Controller, HttpCode, Post, Request, UseGuards } from '@nestjs/common';
import { ApiBody, ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';

import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { RequestWithUser } from '../user/types/request-with-user';
import { ValidateUserDto } from './dto/validate-user.dto';
import { SuccessAuth } from './swagger-types/success-auth';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post()
  @HttpCode(200)
  @ApiOperation({ summary: 'Basic auth' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: SuccessAuth,
  })
  @UseGuards(LocalAuthGuard)
  @ApiBody({ type: ValidateUserDto })
  async login(@Request() req: RequestWithUser) {
    return this.authService.login(req.user);
  }
}
