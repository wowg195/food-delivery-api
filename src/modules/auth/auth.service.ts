import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

import { UserService } from '../user/user.service';
import { ValidateUserDto } from './dto/validate-user.dto';
import { UserEntity } from '../user/schemas/user.schema';
import { IJwtPayload } from './types/jwt-payload.interface';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(validateUserDto: ValidateUserDto) {
    const { phone, password } = validateUserDto;

    const user = await this.userService.findByPhone(phone);
    if (!user) {
      return;
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (isPasswordValid) {
      return user;
    }
  }

  async login(user: UserEntity) {
    const payload: IJwtPayload = { userId: user._id };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
