import { Controller, Post, Body } from '@nestjs/common';
import { ApiTags, ApiExcludeController } from '@nestjs/swagger';

import { IPickupData } from '../iiko/types/pickup-data.interface';
import { CartService } from './cart.service';
import { ICart } from './types/cart.interface';
import { IOrderData } from './types/order-data.interface';

@ApiTags('cart')
@Controller('cart')
@ApiExcludeController()
export class CartController {
  constructor(private readonly cartService: CartService) {}

  @Post('create-order')
  create(
    @Body()
    {
      cart,
      orderData,
      pickupData,
    }: {
      cart: ICart;
      orderData: IOrderData;
      pickupData: IPickupData;
    },
  ) {
    return this.cartService.sendOrder(cart, orderData, pickupData);
  }
}
