import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import {
  ProductEntity,
  ProductSchema,
} from '../products/schemas/product.schema';
import {
  ComponentItemEntity,
  ComponentItemSchema,
} from '../components-items/schemas/component-item.schema';
import {
  ToppingEntity,
  ToppingSchema,
} from '../topping/schemas/topping.schema';
import { CartService } from './cart.service';
import { CartController } from './cart.controller';
import { RedisCacheModule } from '../redis-cache/redis-cache.module';
import { IikoModule } from '../iiko/iiko.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: ProductEntity.name, schema: ProductSchema },
      { name: ToppingEntity.name, schema: ToppingSchema },
      { name: ComponentItemEntity.name, schema: ComponentItemSchema },
    ]),
    RedisCacheModule,
    IikoModule,
  ],
  controllers: [CartController],
  providers: [CartService],
})
export class CartModule {}
