import { ProductEntity } from 'src/modules/products/schemas/product.schema';

export interface ICart {
  response: ProductEntity[];
}
