export interface IOrderData {
  apartment: number;

  change: number;

  city: string;

  deliveryTime: string;

  deliveryType: string;

  district: string;

  door: string;

  entrance: string;

  floor: string;

  house: number;

  name: string;

  numberOfPersons: number;

  paymentMethod: string;

  phone: string;

  street: string;

  userNote: string;

  preOrderDate: string;
}
