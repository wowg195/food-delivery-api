import { Types } from 'mongoose';

import { ImageSchema } from 'src/common/schemas/image.schema';
import { ProductNutritionSchema } from 'src/common/schemas/product-nutrition.schema';
import { ProductParamsSchema } from 'src/common/schemas/product-params.schema';
import { ComponentsCategoryEntity } from 'src/modules/components-categories/schemas/components-categories.schema';
import { IikoModifiersSchema } from 'src/modules/products/schemas/iiko-modifiers.schema';
import { ToppingEntity } from 'src/modules/topping/schemas/topping.schema';

export interface IProduct {
  _id: string;

  title: string;

  iikoId: string;

  notIikoConstructor: boolean;

  urlName: string;

  description: string;

  price: number;

  composition: string;

  params: ProductParamsSchema;

  nutrition: ProductNutritionSchema;

  emoji: string;

  order: number;

  new: boolean;

  orderNew: number;

  simple: boolean;

  details: boolean;

  status: boolean;

  disableOnWeekends: boolean;

  startSaleTime: string;

  endSaleTime: string;

  fixedPrice: boolean;

  images: ImageSchema[];

  iikoModifiers?: IikoModifiersSchema;

  toppings?: ToppingEntity | Types.ObjectId;

  components?: ComponentsCategoryEntity | Types.ObjectId;

  category: Types.ObjectId;
}
