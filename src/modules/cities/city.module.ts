import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { CitiesService } from './city.service';
import { CitiesController } from './city.controller';
import { CityEntity, CitySchema } from './schemas/city.schema';
import { CitiesRepository } from './city.repository';
import { PointsModule } from '../points/points.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: CityEntity.name, schema: CitySchema }]),
    PointsModule,
  ],
  controllers: [CitiesController],
  providers: [CitiesService, CitiesRepository],
  exports: [CitiesService],
})
export class CitiesModule {}
