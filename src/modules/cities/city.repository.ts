import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { EntityRepository } from 'src/database/entity.repository';
import { CityDocument, CityEntity } from './schemas/city.schema';

@Injectable()
export class CitiesRepository extends EntityRepository<CityDocument> {
  constructor(
    @InjectModel(CityEntity.name)
    private citiesModel: Model<CityDocument>,
  ) {
    super(citiesModel);
  }

  async getCities(): Promise<CityEntity[]> {
    return this.citiesModel.find().sort({ order: 'asc' });
  }
}
