import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

import { LocationSchema } from './location.schema';

export type CityDocument = CityEntity & Document;

@Schema({ versionKey: false, collection: 'cities' })
export class CityEntity {
  @ApiProperty()
  _id: string;

  @Prop({ required: true, unique: true })
  @ApiProperty()
  name: string;

  @Prop({ type: () => LocationSchema, _id: false })
  @ApiProperty()
  location: LocationSchema;

  @Prop({ default: 0 })
  @ApiProperty()
  order: number;

  @Prop({ default: true })
  @ApiProperty()
  status: boolean;
}

export const CitySchema = SchemaFactory.createForClass(CityEntity);
