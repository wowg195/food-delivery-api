import { Prop } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

export class LocationSchema {
  @Prop({ default: 'Point' })
  @ApiProperty()
  type: string;

  @Prop({ index: '2dsphere' })
  @ApiProperty()
  coordinates: [number, number];
}
