import { Controller, Get } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse } from '@nestjs/swagger';

import { ComponentsService } from './components.service';
import { ComponentEntity } from './schemas/components.schema';

@ApiTags('components')
@Controller('components')
export class ComponentsController {
  constructor(private readonly componentsService: ComponentsService) {}

  @Get()
  @ApiOperation({ summary: 'Get all product components' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [ComponentEntity],
  })
  async getComponents() {
    return this.componentsService.getComponents();
  }
}
