import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import {
  ComponentEntity,
  ComponentsDocument,
} from './schemas/components.schema';

@Injectable()
export class ComponentsRepository {
  constructor(
    @InjectModel(ComponentEntity.name)
    private componentsRepository: Model<ComponentsDocument>,
  ) {}

  async find() {
    return this.componentsRepository.aggregate([
      {
        $sort: {
          order: -1,
        },
      },
      {
        $lookup: {
          from: 'componentitems',
          localField: 'products',
          foreignField: '_id',
          as: 'products',
        },
      },
    ]);
  }
}
