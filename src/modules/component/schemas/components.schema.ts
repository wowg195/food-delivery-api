import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document, Types } from 'mongoose';

import { ImageSchema } from 'src/common/schemas/image.schema';
import { ProductsCategoryEntity } from 'src/modules/products-categories/schemas/products-categories.schema';
import { ProductsSchema } from './products.schema';

export type ComponentsDocument = ComponentEntity & Document;

@Schema({ versionKey: false, collection: 'components' })
export class ComponentEntity {
  @ApiProperty()
  _id: string;

  @Prop({ required: true })
  @ApiProperty()
  title: string;

  @Prop({ required: true })
  @ApiProperty({ type: ProductsCategoryEntity })
  category: Types.ObjectId;

  @Prop({ type: () => [ProductsSchema], _id: false })
  @ApiProperty({ isArray: true, type: ProductsSchema })
  products: ProductsSchema[];

  @Prop()
  @ApiProperty()
  description: string;

  @Prop()
  @ApiProperty()
  emoji: string;

  @Prop({ type: () => [ImageSchema], _id: false })
  @ApiProperty({ isArray: true, type: ImageSchema })
  images: ImageSchema[];

  @Prop({ default: 0 })
  @ApiProperty()
  order: number;

  @Prop({ default: true })
  @ApiProperty()
  status: boolean;
}

export const ComponentsSchema = SchemaFactory.createForClass(ComponentEntity);
