import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ComponentsCategoriesController } from './components-categories.controller';
import { ComponentsCategoriesRepository } from './components-categories.repository';
import { ComponentsCategoriesService } from './components-categories.service';
import {
  ComponentsCategoryEntity,
  ComponentsCategoriesSchema,
} from './schemas/components-categories.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: ComponentsCategoryEntity.name,
        schema: ComponentsCategoriesSchema,
      },
    ]),
  ],
  controllers: [ComponentsCategoriesController],
  providers: [ComponentsCategoriesService, ComponentsCategoriesRepository],
})
export class ComponentsCategoriesModule {}
