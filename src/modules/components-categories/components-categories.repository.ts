import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { EntityRepository } from 'src/database/entity.repository';
import {
  ComponentsCategoryEntity,
  ComponentsCategoriesDocument,
} from './schemas/components-categories.schema';

@Injectable()
export class ComponentsCategoriesRepository extends EntityRepository<ComponentsCategoriesDocument> {
  constructor(
    @InjectModel(ComponentsCategoryEntity.name)
    private componentsCategoriesRepository: Model<ComponentsCategoriesDocument>,
  ) {
    super(componentsCategoriesRepository);
  }
}
