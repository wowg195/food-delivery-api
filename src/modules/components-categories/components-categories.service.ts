import { Injectable } from '@nestjs/common';

import { ComponentsCategoriesRepository } from './components-categories.repository';

@Injectable()
export class ComponentsCategoriesService {
  constructor(
    private readonly componentsCategoriesRepository: ComponentsCategoriesRepository,
  ) {}

  async getComponentsCategories() {
    return this.componentsCategoriesRepository.find();
  }

  async getComponentsCategoryById(componentsCategoryId: string) {
    return this.componentsCategoriesRepository.findById(componentsCategoryId);
  }
}
