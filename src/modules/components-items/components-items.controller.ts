import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import {
  ApiTags,
  ApiOperation,
  ApiResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';

import { ComponentsItemsService } from './components-items.service';
import { CreateComponentItemDto } from './dto/create-component-item.dto';
import { ComponentItemEntity } from './schemas/component-item.schema';

@ApiTags('components-items')
@Controller('components-items')
export class ComponentsItemsController {
  constructor(
    private readonly componentsItemsService: ComponentsItemsService,
  ) {}

  @Post()
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Create product component item' })
  @ApiResponse({
    status: 201,
    description: 'Success',
    type: [ComponentItemEntity],
  })
  create(@Body() createComponentItemDto: CreateComponentItemDto) {
    return this.componentsItemsService.create(createComponentItemDto);
  }
}
