import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ComponentsItemsController } from './components-items.controller';
import { ComponentsItemsService } from './components-items.service';
import {
  ComponentItemEntity,
  ComponentItemSchema,
} from './schemas/component-item.schema';
import { ComponentsItemsRepository } from './components-items.repository';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: ComponentItemEntity.name, schema: ComponentItemSchema },
    ]),
  ],
  controllers: [ComponentsItemsController],
  providers: [ComponentsItemsService, ComponentsItemsRepository],
})
export class ComponentsItemsModule {}
