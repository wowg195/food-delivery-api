import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { EntityRepository } from 'src/database/entity.repository';
import {
  ComponentItemEntity,
  ComponentItemDocument,
} from './schemas/component-item.schema';

@Injectable()
export class ComponentsItemsRepository extends EntityRepository<ComponentItemDocument> {
  constructor(
    @InjectModel(ComponentItemEntity.name)
    private componentItemModel: Model<ComponentItemDocument>,
  ) {
    super(componentItemModel);
  }
}
