import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsBoolean,
  IsNumber,
  IsObject,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';

import { ProductParamsSchema } from 'src/common/schemas/product-params.schema';

export class CreateComponentItemDto {
  @IsString()
  @ApiProperty()
  readonly title: string;

  @IsNumber()
  @IsOptional()
  @ApiProperty()
  readonly order?: number;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly iikoId?: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly iikoGroupId?: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  readonly status?: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty()
  readonly details?: boolean;

  @IsString()
  @IsOptional()
  @ApiProperty()
  readonly description?: string;

  @IsNumber()
  @ApiProperty()
  readonly price: number;

  @IsObject()
  @ValidateNested()
  @IsOptional()
  @Type(() => ParamsDto)
  @ApiProperty()
  readonly params?: ProductParamsSchema;
}

export class ParamsDto {
  @IsNumber()
  @IsOptional()
  @ApiProperty()
  readonly weight?: number;
}
