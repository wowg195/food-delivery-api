import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

import { ProductNutritionSchema } from 'src/common/schemas/product-nutrition.schema';
import { ProductParamsSchema } from 'src/common/schemas/product-params.schema';

export type ComponentItemDocument = ComponentItemEntity & Document;

@Schema({ versionKey: false, collection: 'componentitems' })
export class ComponentItemEntity {
  @ApiProperty()
  _id: string;

  @Prop()
  @ApiProperty()
  title: string;

  @Prop({ default: 0 })
  @ApiProperty()
  order: number;

  @Prop()
  @ApiProperty()
  iikoId: string;

  @Prop()
  @ApiProperty()
  iikoGroupId: string;

  @Prop({ default: true })
  @ApiProperty()
  status: boolean;

  @Prop()
  @ApiProperty()
  details: boolean;

  @Prop()
  @ApiProperty()
  description: string;

  @Prop()
  @ApiProperty()
  price: number;

  @Prop({ type: () => ProductParamsSchema, _id: false })
  @ApiProperty()
  params: ProductParamsSchema;

  @Prop({ type: () => ProductNutritionSchema, _id: false })
  @ApiProperty()
  nutrition: ProductNutritionSchema;
}

export const ComponentItemSchema =
  SchemaFactory.createForClass(ComponentItemEntity);
