import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ContentsController } from './contents.controller';
import { ContentsRepository } from './contents.repository';
import { ContentsService } from './contents.service';
import { ContentEntity, ContentsSchema } from './schemas/contents.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: ContentEntity.name, schema: ContentsSchema },
    ]),
  ],
  controllers: [ContentsController],
  providers: [ContentsService, ContentsRepository],
})
export class ContentsModule {}
