import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { ContentEntity, ContentsDocument } from './schemas/contents.schema';

@Injectable()
export class ContentsRepository {
  constructor(
    @InjectModel(ContentEntity.name)
    private contentsRepository: Model<ContentsDocument>,
  ) {}

  async find() {
    return this.contentsRepository.find();
  }
}
