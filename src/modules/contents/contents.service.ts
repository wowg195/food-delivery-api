import { Injectable } from '@nestjs/common';

import { ContentsRepository } from './contents.repository';

@Injectable()
export class ContentsService {
  constructor(private readonly contentsRepository: ContentsRepository) {}

  async getContents() {
    return this.contentsRepository.find();
  }
}
