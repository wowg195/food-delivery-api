import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export type ContentsDocument = ContentEntity & Document;

@Schema({ versionKey: false, collection: 'contents' })
export class ContentEntity {
  @ApiProperty()
  _id: string;

  @Prop({ required: true })
  @ApiProperty()
  title: string;

  @Prop({ default: true })
  @ApiProperty()
  status: boolean;

  @Prop({ default: 0 })
  @ApiProperty()
  order: number;

  @Prop({ required: true })
  @ApiProperty()
  content: string;

  @Prop({ required: true })
  @ApiProperty()
  key: string;
}

export const ContentsSchema = SchemaFactory.createForClass(ContentEntity);
