import { Module } from '@nestjs/common';

import { IikoModule } from '../iiko/iiko.module';
import { CronService } from './cron.service';

@Module({
  imports: [IikoModule],
  providers: [CronService],
})
export class CronModule {}
