import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

import { IikoService } from '../iiko/iiko.service';

@Injectable()
export class CronService {
  constructor(private readonly iikoService: IikoService) {}

  @Cron('*/45 * * * *')
  async iikoAuth() {
    return this.iikoService.iikoAuth();
  }
}
