import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { ensureDir, writeFile } from 'fs-extra';
import { path } from 'app-root-path';
import { v4 as uuidv4 } from 'uuid';
import * as sharp from 'sharp';

import { FileFolder, FileType } from './types/file.enum';
import { FileErrors } from './types/file.errors';
import { PUBLIC_FOLDER_PATH } from 'src/config/config';

@Injectable()
export class FilesService {
  async saveImage(file: Express.Multer.File) {
    try {
      const uploadFolder = `${path}${FileFolder.UPLOADS}`;
      ensureDir(uploadFolder);

      const fileName = uuidv4();
      const filePath = `${uploadFolder}/${fileName}.${FileType.JPG}`;
      const jpgFileBuffer = await sharp(file.buffer).jpeg().toBuffer();

      await writeFile(filePath, jpgFileBuffer);

      return [
        {
          src: `${PUBLIC_FOLDER_PATH}/${fileName}.${FileType.JPG}`,
          ext: FileType.JPG,
        },
      ];
    } catch (error) {
      throw new InternalServerErrorException(FileErrors.SAVE_FILE);
    }
  }
}
