export enum FileErrors {
  NO_IMAGE = 'Отсутствует изображение',
  SAVE_FILE = 'Ошибка при сохранении файла',
}
