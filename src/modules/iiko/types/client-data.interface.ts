export interface IClientData {
  id: string;
  referrerId?: string;
  name: string;
  surname: string;
  middleName: string;
  comment: string;
  phone: string;
  cultureName: string;
  birthday: string;
  email: string;
  sex: 0 | 1 | 2;
  consentStatus: 0 | 1 | 2;
  anonymized: boolean;
  userData: string;
  shouldReceivePromoActionsInfo: boolean;
  shouldReceiveLoyaltyInfo: boolean;
  shouldReceiveOrderStatusInfo: boolean;
  personalDataConsentFrom: string;
  personalDataConsentTo: string;
  personalDataProcessingFrom: string;
  personalDataProcessingTo: string;
  isDeleted: false;
}
