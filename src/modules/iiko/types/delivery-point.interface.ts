export interface IDeliveryPoint {
  address: {
    street: {
      name: string;
    };
    house: number;
    flat: number;
    floor?: string;
    door?: string;
    entrance?: string;
  };
}
