import { ICart } from 'src/modules/cart/types/cart.interface';
import { IikoOrderServiceType } from '../enums/iiko.enum';
import { IDeliveryPoint } from './delivery-point.interface';

export type IikoOrder = {
  organizationId: string;
  order: {
    orderServiceType: IikoOrderServiceType;
    phone: string;
    comment: string;
    customer: {
      name: string;
    };
    items: ICart;
    completeBefore?: string;
    deliveryPoint?: IDeliveryPoint;
  };
};
