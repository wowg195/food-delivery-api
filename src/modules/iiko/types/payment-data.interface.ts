export interface IPaymentData {
  change: number;
  paymentMethod: string;
}
