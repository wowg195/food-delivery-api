import { Injectable } from '@nestjs/common';
import { Types } from 'mongoose';

import { PointsRepository } from './points.repository';

@Injectable()
export class PointsService {
  constructor(private readonly pointsRepository: PointsRepository) {}

  async findAll() {
    return this.pointsRepository.getPoints();
  }

  async getPointsByCity(cityId: Types.ObjectId) {
    return this.pointsRepository.find({ city: cityId });
  }

  async getPointByAddress(address: string) {
    return this.pointsRepository.findOne({ address });
  }
}
