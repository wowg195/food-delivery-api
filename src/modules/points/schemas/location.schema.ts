import { Prop } from '@nestjs/mongoose';

export class LocationSchema {
  @Prop({ default: 'Point' })
  type: string;

  @Prop({ index: '2dsphere' })
  coordinates: [number];
}
