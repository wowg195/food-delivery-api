export enum PointErrors {
  NOT_FOUND = 'Магазин не найден',
  NOT_FOUND_POINTS_FOR_CITY = 'Не найдены магазины для данного города',
}
