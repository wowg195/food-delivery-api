import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ProductsCategoriesController } from './products-categories.controller';
import { ProductsCategoriesRepository } from './products-categories.repository';
import { ProductsCategoriesService } from './products-categories.service';
import {
  ProductsCategoryEntity,
  ProductsCategoriesSchema,
} from './schemas/products-categories.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: ProductsCategoryEntity.name, schema: ProductsCategoriesSchema },
    ]),
  ],
  controllers: [ProductsCategoriesController],
  providers: [ProductsCategoriesService, ProductsCategoriesRepository],
  exports: [ProductsCategoriesService],
})
export class ProductsCategoriesModule {}
