import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { EntityRepository } from 'src/database/entity.repository';
import {
  ProductsCategoryEntity,
  ProductsCategoriesDocument,
} from './schemas/products-categories.schema';

@Injectable()
export class ProductsCategoriesRepository extends EntityRepository<ProductsCategoriesDocument> {
  constructor(
    @InjectModel(ProductsCategoryEntity.name)
    private productsCategoriesModel: Model<ProductsCategoriesDocument>,
  ) {
    super(productsCategoriesModel);
  }

  async findProductsCategories(): Promise<ProductsCategoryEntity[]> {
    return this.productsCategoriesModel
      .find({ status: true })
      .sort({ order: 'desc' });
  }

  async findProductCategoryByUrlOrId(
    queryString: string,
  ): Promise<ProductsCategoryEntity | null> {
    return this.productsCategoriesModel.findOne({
      $or: [{ _id: queryString }, { urlName: queryString }],
    });
  }
}
