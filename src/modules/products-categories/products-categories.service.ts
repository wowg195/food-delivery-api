import { BadRequestException, Injectable } from '@nestjs/common';

import { CreateProductCategoryDto } from './dto/createProductCategory.dto';
import { ProductsCategoriesRepository } from './products-categories.repository';
import { ProductsCategoriesErrors } from './types/products-categories.errors';

@Injectable()
export class ProductsCategoriesService {
  constructor(
    private readonly productsCategoriesRepository: ProductsCategoriesRepository,
  ) {}

  async getProductsCategories() {
    return this.productsCategoriesRepository.findProductsCategories();
  }

  async getProductCategoryById(productCategoryId: string) {
    return this.productsCategoriesRepository.findById(productCategoryId);
  }

  async createProductCategory(
    createProductCategoryDto: CreateProductCategoryDto,
  ) {
    const { urlName } = createProductCategoryDto;
    const categoryWithSameUrlName = await this.getProductCategoryByUrl(urlName);
    if (categoryWithSameUrlName) {
      throw new BadRequestException(ProductsCategoriesErrors.EXISTING_URL_NAME);
    }

    return this.productsCategoriesRepository.create(createProductCategoryDto);
  }

  async getProductCategoryByUrlOrId(queryString: string) {
    return this.productsCategoriesRepository.findProductCategoryByUrlOrId(
      queryString,
    );
  }
  async getProductCategoryByUrl(urlName: string) {
    return this.productsCategoriesRepository.findOne({ urlName });
  }
}
