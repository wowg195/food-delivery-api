import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export type ProductsCategoriesDocument = ProductsCategoryEntity & Document;

@Schema({ versionKey: false, collection: 'productscategories' })
export class ProductsCategoryEntity {
  @ApiProperty()
  _id: string;

  @Prop({ required: true })
  @ApiProperty()
  title: string;

  @Prop()
  @ApiProperty()
  description: string;

  @Prop({ unique: true, lowercase: true })
  @ApiProperty()
  urlName: string;

  @Prop()
  @ApiProperty()
  emoji: string;

  @Prop()
  @ApiProperty()
  icon: string;

  @Prop({ default: 0 })
  @ApiProperty()
  order: number;

  @Prop({ default: true })
  @ApiProperty()
  status: boolean;
}

export const ProductsCategoriesSchema = SchemaFactory.createForClass(
  ProductsCategoryEntity,
);
