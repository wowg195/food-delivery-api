export enum ProductsCategoriesErrors {
  NOT_FOUND = 'Категория не найдена',
  EXISTING_URL_NAME = 'Категория с таким url уже существует',
}
