import { Prop } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';

import { ModifierItemSchema } from './modifier-item.schema';

export class IikoModifiersSchema {
  @Prop()
  @ApiProperty()
  wasabi: ModifierItemSchema;

  @Prop()
  @ApiProperty()
  ginger: ModifierItemSchema;

  @Prop()
  @ApiProperty()
  soy_sauce: ModifierItemSchema;
}
