import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Types } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

import { ToppingEntity } from 'src/modules/topping/schemas/topping.schema';
import { ToppingsCategoryEntity } from 'src/modules/topping-categories/schemas/topping-category.schema';
import { ComponentsCategoryEntity } from 'src/modules/components-categories/schemas/components-categories.schema';
import { IikoModifiersSchema } from './iiko-modifiers.schema';
import { ProductNutritionSchema } from 'src/common/schemas/product-nutrition.schema';
import { ProductParamsSchema } from 'src/common/schemas/product-params.schema';
import { ImageSchema } from 'src/common/schemas/image.schema';
import { ProductsCategoryEntity } from 'src/modules/products-categories/schemas/products-categories.schema';

export type ProductDocument = ProductEntity & Document;

@Schema({ versionKey: false, collection: 'products' })
export class ProductEntity {
  @ApiProperty()
  _id: string;

  @Prop({ required: true })
  @ApiProperty()
  title: string;

  @Prop()
  @ApiProperty()
  iikoId: string;

  @Prop()
  @ApiProperty()
  notIikoConstructor: boolean;

  @Prop({ unique: true, lowercase: true })
  @ApiProperty()
  urlName: string;

  @Prop()
  @ApiProperty()
  description: string;

  @Prop({ required: true })
  @ApiProperty()
  price: number;

  @Prop()
  @ApiProperty()
  composition: string;

  @Prop({ type: () => ProductParamsSchema, _id: false })
  @ApiProperty()
  params: ProductParamsSchema;

  @Prop({ type: () => ProductNutritionSchema, _id: false })
  @ApiProperty()
  nutrition: ProductNutritionSchema;

  @Prop()
  @ApiProperty()
  emoji: string;

  @Prop({ default: 0 })
  @ApiProperty()
  order: number;

  @Prop()
  @ApiProperty()
  new: boolean;

  @Prop({ default: 0 })
  @ApiProperty()
  orderNew: number;

  @Prop({ default: false })
  @ApiProperty()
  simple: boolean;

  @Prop({ default: false })
  @ApiProperty()
  details: boolean;

  @Prop({ default: true })
  @ApiProperty()
  status: boolean;

  @Prop({ default: false })
  @ApiProperty()
  disableOnWeekends: boolean;

  @Prop()
  @ApiProperty()
  startSaleTime: string;

  @Prop()
  @ApiProperty()
  endSaleTime: string;

  @Prop()
  @ApiProperty()
  fixedPrice: boolean;

  @Prop({ type: () => [ImageSchema], _id: false })
  @ApiProperty({ isArray: true, type: ImageSchema })
  images: ImageSchema[];

  @Prop({ type: () => IikoModifiersSchema, _id: false })
  @ApiProperty()
  iikoModifiers: IikoModifiersSchema;

  // topping
  @Prop({ type: Types.ObjectId, ref: ToppingsCategoryEntity.name })
  @ApiProperty({ type: ToppingEntity })
  toppings: ToppingEntity;

  // components
  @Prop({
    type: Types.ObjectId,
    ref: ComponentsCategoryEntity.name,
  })
  @ApiProperty({ type: [ComponentsCategoryEntity] })
  components: ComponentsCategoryEntity;

  // category
  @Prop({
    type: Types.ObjectId,
  })
  @ApiProperty({ type: ProductsCategoryEntity })
  category: Types.ObjectId;
}

export const ProductSchema = SchemaFactory.createForClass(ProductEntity);
