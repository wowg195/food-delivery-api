import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cache } from 'cache-manager';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class RedisCacheService {
  constructor(
    @Inject(CACHE_MANAGER) private readonly cache: Cache,
    private readonly configService: ConfigService,
  ) {}

  async get(key: string): Promise<any> {
    return this.cache.get(key);
  }

  async set(key: string, value: any) {
    await this.cache.set(key, value);
  }

  async setWithEncryption(key: string, value: string) {
    const encryption_key = this.configService.get('REDIS_ENCRYPTION_KEY');

    const encryptedValue = CryptoJS.AES.encrypt(
      value,
      encryption_key,
    ).toString();

    await this.cache.set(key, encryptedValue);
  }

  async getWithEncryption(key: string): Promise<string> {
    const encryption_key = this.configService.get('REDIS_ENCRYPTION_KEY');

    const value = (await this.cache.get(key)) as string;

    const bytes = CryptoJS.AES.decrypt(value, encryption_key);
    const decryptedValue = bytes.toString(CryptoJS.enc.Utf8);

    return decryptedValue;
  }

  async reset() {
    await this.cache.reset();
  }

  async del(key: string) {
    await this.cache.del(key);
  }
}
