import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';

import { RequestManagerService } from './request-manager.service';

@Module({
  imports: [HttpModule],
  providers: [RequestManagerService],
  exports: [RequestManagerService],
})
export class RequestManagerModule {}
