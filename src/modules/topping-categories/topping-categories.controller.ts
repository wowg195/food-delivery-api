import { Controller, Get, Param } from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiParam } from '@nestjs/swagger';

import { ToppingsCategoryEntity } from './schemas/topping-category.schema';
import { ToppingCategoriesService } from './topping-categories.service';

@ApiTags('toppings-categories')
@Controller('toppings-categories')
export class ToppingCategoriesController {
  constructor(
    private readonly toppingCategoriesService: ToppingCategoriesService,
  ) {}

  @Get()
  @ApiOperation({ summary: 'Get toppings categories' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [ToppingsCategoryEntity],
  })
  async getToppingCategories() {
    return this.toppingCategoriesService.getToppingCategories();
  }

  @Get(':toppingCategory')
  @ApiOperation({ summary: 'Get toppings category by id' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: ToppingsCategoryEntity,
  })
  @ApiParam({
    name: 'toppingCategory',
    required: true,
    description: 'Topping category id',
  })
  async getToppingCategoryById(
    @Param('toppingCategory') toppingCategory: string,
  ) {
    return this.toppingCategoriesService.getToppingCategoryById(
      toppingCategory,
    );
  }
}
