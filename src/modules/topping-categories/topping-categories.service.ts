import { Injectable } from '@nestjs/common';

import { ToppingsCategoriesRepository } from './toppingsCategories.repository';

@Injectable()
export class ToppingCategoriesService {
  constructor(
    private readonly toppingsCategoriesRepository: ToppingsCategoriesRepository,
  ) {}

  async getToppingCategories() {
    return this.toppingsCategoriesRepository.find();
  }

  async getToppingCategoryById(toppingCategory: string) {
    return this.toppingsCategoriesRepository.findById(toppingCategory);
  }
}
