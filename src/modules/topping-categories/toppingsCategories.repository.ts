import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { EntityRepository } from 'src/database/entity.repository';
import {
  ToppingsCategoryEntity,
  ToppingsCategoriesDocument,
} from './schemas/topping-category.schema';

@Injectable()
export class ToppingsCategoriesRepository extends EntityRepository<ToppingsCategoriesDocument> {
  constructor(
    @InjectModel(ToppingsCategoryEntity.name)
    private toppingModel: Model<ToppingsCategoriesDocument>,
  ) {
    super(toppingModel);
  }
}
