import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsOptional, IsString, Min } from 'class-validator';

export class CreateToppingDto {
  @IsNumber()
  @Min(1)
  @ApiProperty({ default: 1 })
  price: number;

  @IsString()
  @ApiProperty()
  title: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  iikoId?: string;
}
