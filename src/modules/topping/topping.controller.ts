import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import {
  ApiOperation,
  ApiTags,
  ApiResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';

import { CreateToppingDto } from './dto/create-topping.dto';
import { ToppingEntity } from './schemas/topping.schema';
import { ToppingService } from './topping.service';

@ApiTags('toppings')
@Controller('toppings')
export class ToppingController {
  constructor(private readonly toppingService: ToppingService) {}

  @Get()
  @ApiOperation({ summary: 'Get all toppings' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [ToppingEntity],
  })
  async getToppings() {
    return this.toppingService.getToppings();
  }

  @Post()
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Create topping' })
  @ApiResponse({
    status: 201,
    description: 'Success',
    type: ToppingEntity,
  })
  async createTopping(@Body() topping: CreateToppingDto) {
    return this.toppingService.createTopping(topping);
  }
}
