import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ToppingEntity, ToppingSchema } from './schemas/topping.schema';
import { ToppingController } from './topping.controller';
import { ToppingRepository } from './topping.repository';
import { ToppingService } from './topping.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: ToppingEntity.name, schema: ToppingSchema },
    ]),
  ],
  controllers: [ToppingController],
  providers: [ToppingService, ToppingRepository],
})
export class ToppingModule {}
