import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Exclude, Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';

export type UserDocument = UserEntity & Document;

@Schema({ versionKey: false, collection: 'users' })
export class UserEntity {
  @ApiProperty()
  @Transform(({ value }) => value.toString())
  _id: string;

  @Prop({ required: true })
  @ApiProperty()
  name: string;

  @Prop()
  @ApiProperty()
  secondName: string;

  @Prop({ required: true })
  @ApiProperty()
  @Exclude()
  password: string;

  @Prop({ required: true, unique: true })
  @ApiProperty()
  phone: string;

  @Prop({ required: true, unique: true, trim: true })
  @ApiProperty()
  email: string;
}

export const UserSchema = SchemaFactory.createForClass(UserEntity);
