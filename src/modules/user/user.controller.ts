import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags, ApiOperation, ApiResponse, ApiParam } from '@nestjs/swagger';

import MongooseClassSerializerInterceptor from 'src/common/interceptors/mongooseClassSerializer.interceptor';
import { CreateUserDto } from './dto/create-user.dto';
import { UserEntity } from './schemas/user.schema';
import { SwaggerUserEntity } from './swagger-types/user-without-password';
import { UserService } from './user.service';

@ApiTags('users')
@Controller('users')
@UseInterceptors(MongooseClassSerializerInterceptor(UserEntity))
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get()
  @ApiOperation({ summary: 'Get all users' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: [SwaggerUserEntity],
  })
  findAll() {
    return this.userService.findAll();
  }

  @Get(':userId')
  @ApiOperation({ summary: 'Get user by id' })
  @ApiResponse({
    status: 200,
    description: 'Success',
    type: SwaggerUserEntity,
  })
  @ApiParam({ name: 'userId', required: true, description: 'User id' })
  findById(@Param('userId') userId: string) {
    return this.userService.findById(userId);
  }

  @Post()
  @ApiOperation({ summary: 'Create new user' })
  @ApiResponse({
    status: 201,
    description: 'Success',
    type: SwaggerUserEntity,
  })
  create(@Body() createUSerDto: CreateUserDto) {
    return this.userService.create(createUSerDto);
  }
}
